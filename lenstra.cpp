#include "int_types.hpp"
#include "montgomeryEC.hpp"
#include "arithm.hpp"
#include "mod_int.hpp"


#include <ctime>
#include <chrono>
#include <vector>
#include <cstdlib>
#include <sstream>


typedef mpz_class bint;


#define maxN 10000000

bool isCompose[maxN] = {};
uint64_t prime[maxN] = {};


#ifdef USE_MPI
#include <mpi.h>
#include <signal.h>
#include <unistd.h>

int world_size;
int world_rank ;
char data[1024];
#endif

uint64_t cribleEratostene(uint64_t N=maxN) {
    uint64_t c = 0;
    for (uint64_t p = 2; p < N; p++) {
        if (!isCompose[p]) {
            prime[c++] = p;
            for (uint64_t k = 2 * p; k < N; k += p)
                isCompose[k] = true;
        }
    }
    prime[c] = 0;
    return c;
}

uint64_t B(double logp) {
    return (uint64_t)exp(1/sqrt(2) * sqrt(logp*log(logp)));
}

bint ECMTrial(bint const& N, mpz_class const& k1, mpz_class const& k2) {
    mod_int<bint> sg(randint(bint(5), N));
    mod_int<bint> u = sg * sg - mod_int<bint>(5), v = 4 * sg;
    mod_int<bint> vmu = v - u, uc = u * u * u, vc = v * v * v;
    bint gcd = pgcd(bint(abs((4*uc*v).get_v())), N);
    if (gcd > 1 && gcd < N) {
        std::cout << "Found factor " << gcd << " trying to generate a number" << std::endl;
        return gcd;
    }
    gcd = pgcd(bint(abs(EC<mod_int<bint>>::delta().get_v())), N);
    if (gcd > 1 && gcd < N) {
        std::cout << "Found factor " << gcd << " trying to generate a curve" << std::endl;
        return gcd;
    }
    EC<mod_int<bint>>::init(vmu*vmu*vmu*(3*u+v)/(4*uc*v)-bint(2), bint(1));
    EC<mod_int<bint>>::xPoint P = EC<mod_int<bint>>::xPoint(uc, vc);
    // std::cout << "trying: Point: " << P << " on EC: " << EC<mod_int<bint>>::strm << std::endl;
    P = k1 * P;
    gcd = pgcd(bint(abs(P.Z.get_v())), N);
    if (gcd == 1) {
        P = k2 * P;
        gcd = pgcd(bint(abs(P.Z.get_v())), N);
    }
    if (gcd == 0 || gcd == N) { std::cout << "you went too far" << std::endl; return 0; }
    else if (gcd > 1 && gcd < N) {
        std::cout << "Found factor " << gcd << " using " << P << " on " << EC<mod_int<bint>>::strm << std::endl;
        std::stringstream cmds;
        cmds << "/usr/bin/env python3 ./send_mail.py " << "\"" << "Trying to factorize " << N << " Found factor " << gcd << " using " << P << " on " << EC<mod_int<bint>>::strm << "\"";
        std::string cmd(cmds.str());
        system(cmd.c_str());
        return gcd;
    } else return 0;
}

bint factor(bint N) {
    mod_int<bint>::set_n(N);
    uint64_t logp = 1./2. * log(2) * (double)log2(N);
    uint64_t B1 = B(logp);
    mpz_class k1 = 1;
    uint64_t c = 0;
    for (; prime[c] <= B1; c++) {
        uint64_t p = prime[c];
        uint64_t pv = 1;
        while (pv * p <= B1)
            pv *= p;
        k1 *= pv;
    }
    // uint64_t B2 = 10 * B1;
    mpz_class k2 = 1;
    // for (; prime[c] <= B2; c++) k2 *= prime[c];
#ifndef USE_MPI
    std::cout << "B1=" << B1 << std::endl;
    uint64_t nbtrials = 0;
    while (true) {
        nbtrials++;
        bint f = ECMTrial(N, k1, k2);
        if (f) return f;
        if (nbtrials % 1 == 0) std::cout << nbtrials << " trials" << std::endl;
    }
#else
    if (world_rank == 1) std::cout << "B1=" << B1 << std::endl;
    MPI_Request request;
    MPI_Irecv(0, 0, MPI_CHAR, 0, 0, MPI_COMM_WORLD, &request);
    int flag;
    uint64_t nbtrials = 0;
    do {
        nbtrials++;
        bint f = ECMTrial(N, k1, k2);
        if (f) {
            get_str(data, f);
            MPI_Send(data, 1024, MPI_CHAR, 0, 0, MPI_COMM_WORLD);
            return 0;
        }
        if (nbtrials % 1 == 0) std::cout << nbtrials << " trials" << std::endl;
        MPI_Test(&request, &flag, MPI_STATUS_IGNORE);
    } while (!flag);
    MPI_Send("\0", 1, MPI_CHAR, 0, 0, MPI_COMM_WORLD);
    return 0;
#endif
}

std::vector<bint> factorize(bint N) {
    std::vector<bint> factors;
    for (unsigned int c = 0; N > 1 && prime[c] && prime[c] <= sqrt(N); c++) {
        uint64_t p = prime[c];
        while (N % p == 0) {
            factors.push_back(bint(p));
            N /= p;
        }
    }
    while (N > 1 && !is_pseudo_prime(N)) {
        std::cout << "factorizing " << N << std::endl;
        std::vector<bint> fnd_factors;
#ifndef USE_MPI
        fnd_factors.push_back(factor(N));
#else
        if (log10(N) + 2 > 1024) {
            strcpy(data, "quit");
            MPI_Bcast(data, sizeof("quit"), MPI_CHAR, 0, MPI_COMM_WORLD);
            std::cout << "Dépassement de capacité" << std::endl;
        }
        get_str(data, N);
        MPI_Bcast(data, 1024, MPI_CHAR, 0, MPI_COMM_WORLD);
        for (int i = 1; i < world_size; i++) {
            MPI_Status status;
            MPI_Recv(data, sizeof(data), MPI_CHAR, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);
            if (*data != '\0') fnd_factors.push_back(bint(data));
            if (i == 1) {
                for (int i = 1; i < world_size; i++) {
                    MPI_Request request;
                    MPI_Isend(0, 0, MPI_CHAR, i, 0, MPI_COMM_WORLD, &request);
                }
            }
        }
        MPI_Barrier(MPI_COMM_WORLD);
#endif
        for (bint f : fnd_factors) {
            if (bint(N % f)) continue;
            std::vector<bint> ffactors = factorize(f);
            factors.insert(factors.end(), ffactors.begin(), ffactors.end());
            N /= f;
        }
    }
    if (N != 1) factors.push_back(N);
    return factors;
}

template<> bint mod_int<bint>::n = bint();
template<> mod_int<bint> EC<mod_int<bint>>::A = mod_int<bint>();
template<> mod_int<bint> EC<mod_int<bint>>::B = mod_int<bint>();
template<> mod_int<bint> EC<mod_int<bint>>::Ap2s4 = mod_int<bint>();


#include <iostream>
using namespace std;
using namespace std::chrono;

#ifndef USE_MPI
int main(int argc, char *argv[]) {
    rc.seed(time_point_cast<milliseconds>(system_clock::now()).time_since_epoch().count());

    bint N;
    if (argc > 1) N  = bint(argv[1]);
    else cin >> N;

    cribleEratostene();
    for (bint d : factorize(N)) cout << d << " ";
    std::cout << std::endl;
}
#else

void exit_handler(int s) {
    MPI_Abort(MPI_COMM_WORLD, 1);
}
int main(int argc, char *argv[]) {
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    struct sigaction exitHandler;
    exitHandler.sa_handler = exit_handler;
    sigemptyset(&exitHandler.sa_mask);
    exitHandler.sa_flags = 0;
    sigaction(SIGINT, &exitHandler, NULL);
    
    rc.seed((world_rank+1)*time_point_cast<milliseconds>(system_clock::now()).time_since_epoch().count());
    if(!world_rank) {
        bint N;
        if (argc > 1) N  = bint(argv[1]);
        else cin >> N;
        cribleEratostene();
        for (bint d : factorize(N)) cout << d << " ";
        strcpy(data, "quit");
        MPI_Bcast(data, sizeof("quit"), MPI_CHAR, 0, MPI_COMM_WORLD);
        std::cout << std::endl;
    }
    else {
        cribleEratostene();
        while (true) {
            MPI_Bcast(data, sizeof(data), MPI_CHAR, 0, MPI_COMM_WORLD);
            if (!strcmp(data, "quit")) break;
            else {
                bint N(data);
                factor(N);
            }
            MPI_Barrier(MPI_COMM_WORLD);
        }
    }
    MPI_Finalize();
}
#endif
