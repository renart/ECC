#pragma once

#include "arithm.hpp"
#include "dual_num.hpp"
#include "modp2_int.hpp"
#include "weierstrassEC.hpp"

#include <iostream>

using namespace std;

// T is the type of the local ring elements
// P is the type of the field
template <typename T, typename P> class EC_local_ring
{
public:
    static T a;
    static T b;
    static ostream &strm(ostream & os)
    {
        os << "y**2=x**3+" << a << "x+" << b;
        return os;
    }
    static T delta(){ return 4 * a*a*a - 27 * b*b; }
    static EC<P> projection{
      c = EC<P>::b=();
      c.b = b.get_a()
      
    }
    class ProjectivePoint {
    public:
        T X, Y, Z;
        ProjectivePoint() : X(0), Y(1), Z(0) {}
        ProjectivePoint(const T& X, const T& Y, const T& Z) : X(X), Y(Y), Z(Z) {}
        EC<P> project(){return EC<P>::ProjectivePoint(X.get_a(), Y.get_a(), Z.get_a()); }
        ProjectivePoint& operator=(ProjectivePoint const& P) { X = P.X; Y = P.Y; Z = P.Z; return *this; };
        explicit operator bool() const { return Z == 0; }
        bool operator==(ProjectivePoint const& P) const { return X * P.Z == P.X * Z && Y * P.Z == P.Y * Z; };
        ProjectivePoint add1(ProjectivePoint const& P1, ProjectivePoint const& P2) const {
          T p, q, r;
          p = P1.y*P1.y*P2.x*P2.z−P1.z*P1.x*P2.y*P2.y−a*(P1.z*P2.x+P1.x*P2.z)*(P1.z*P2.x−P1.x*P2.z)+(2*P1.y*P2.y−3*b*P1.z*P2.z)*(P1.z*P2.x−P1.x*P2.z);
          q = P1.y*P2.y*(P2.z*P1.y−P1.z*P2.y)−a*(P1.x*P1.y*P2.z*P2.z−P1.z*P1.z*P2.x*P2.y)+(−2*a*P1.z*P2.z−3*P1.x*P2.x)*(P2.x*P1.y−P1.x*P2.y)−3*b*P1.z*P2.z*(P2.z*P1.y-P1.z*P2.y);
          r = (P1.z*P2.y+P2.z*P1.y)*(P2.z*P1.y−P1.z*P2.y)+(3*P1.x*P2.x+a*p1.z*P2.z)*(P1.z*P2.x−P1.x*P2.z);
          return ProjectivePoint(p, q, r)
        }
        ProjectivePoint add2() const {
            T p, q, r;
            p = (P1.y*P2.y−6*b*P1.z*P2.z)*(P2.x*P1.y+P1.x*P2.y)+(a*a*P1.z*P2.z−2*a*P1.x*P2.x)*(P1.z*P2.y+P2.z*P1.y)−3*b*(P1.x*P1.y*P2.z*P2.z+P1.z*P1.z*P2.x*P2.y)−a*(P1.y*P1.z*P2.x*P2.x+P1.x*P1.x*P2.y*P2.z);
            q = P1.y*P1.y*P2.y*P2.y+3*a*P1.x*P1.x*P2.x*P2.x+(−a*a*a−9*b2)*P1.z*P1.z*P2.z*P2.z−a*a*(P1.z*P2.x+P1.x*P2.z)*2−2*a*a*P1.z*P1.x*P2.z*P2.x+(9*b*P1.x*P2.x−3*a*b*P1.z*P2.z)*(P1.z*P2.x+P1.x*P2.z);
            r = (P1.y*P2.y+3*b*P1.z*P2.z)*(P1.z*P2.y+P2.z*P1.y)+(3*P1.x*P2.x+2*a*P1.z*P2.z)*(P2.x*P1.y+P1.x*P2.y)+a*(P1.x*P1.y*P2.z*P2.z+P1.z*P1.z*P2.x*P2.y);
            return ProjectivePoint(p, q, r)          
        }
        
        friend ProjectivePoint operator+ (ProjectivePoint const& P1, ProjectivePoint const& P2)
        { 
            if (P1.project() == P2.project())
              return add2(P1, P2);
            return add1(P1, P2);
        }
        friend ProjectivePoint operator*(T const& lb, ProjectivePoint& P) { return fast_mult<T, ProjectivePoint>(lb, P); }
        friend ostream &operator<<(ostream& os, const ProjectivePoint& P)
        {
            os << "(X=" << P.X << " : Y=" << P.Y << " : Z=" << P.Z << ")";
            return os;
        }
    };

};

template <typename T>
using EC_Z_p2Z = EC_local_ring<modp2_int<T>, mod_int2<T>>;

template <typename T>
using EC_dual_Fp = EC_local_ring<dual_num<T>, mod_int2<T>>;
