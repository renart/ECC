#pragma once

#include "mod_int.hpp"
#include "arithm.hpp"

#include <iostream>

using namespace std;

template <typename T> class EC
{
public:
    static T a;
    static T b;
    static ostream &strm(ostream & os)
    {
        os << "y**2=x**3+" << a << "x+" << b;
        return os;
    }
    static T delta(){ return 4 * a*a*a - 27 * b*b; }

    class AffinePoint {
    public:
        mod_int<T> x;
        mod_int<T> y;
        bool infty;
        AffinePoint() : x(0), y(0), infty(true) {}
        AffinePoint(bool infty) : x(0), y(0), infty(infty) {}
        AffinePoint(const T& x, const T& y) : x(x), y(y), infty(false) {}
        AffinePoint(const mod_int<T>& x, const mod_int<T>& y) : x(x), y(y), infty(false) {}
        AffinePoint& operator=(AffinePoint const& P) { infty = P.infty; if (!P.infty) { x = P.x; y = P.y; }; return *this; };
        bool operator==(AffinePoint const& P) const { return (infty && P.infty) || (!infty && !P.infty && x == P.x && y == P.y); };
        friend AffinePoint operator+ (AffinePoint const& P1, AffinePoint const& P2)
        {
            if (P1.infty) return AffinePoint(P2);
            if (P2.infty) return AffinePoint(P1);
            if (P1.x != P2.x)
            {
                mod_int<T> lambda = (P2.y - P1.y) / (P2.x - P1.x);
                mod_int<T> x_r = lambda * lambda - P1.x - P2.x;
                return AffinePoint(x_r, lambda*(P1.x - x_r) - P1.y);
            }
            else
            {
                if (P1.y == P2.y)
                {
                    if (P1.y == mod_int<T>(0)) return AffinePoint();
                    mod_int<T> lambda = (3 * P1.x*P1.x + a) / (2 * P1.y);
                    mod_int<T> x_r = lambda * lambda - P1.x - P2.x;
                    return AffinePoint(x_r, lambda*(P1.x - x_r) - P1.y);
                }
                if (P1.y + P2.y == mod_int<T>(0)) return AffinePoint();
                throw 0;
            }
        }
        friend AffinePoint operator*(T const& lb, AffinePoint P) { return fast_mult<T, AffinePoint>(lb, P); }
        friend ostream &operator<<(ostream& os, const AffinePoint& P)
        {
            if (P.infty) os << "O";
            else os << "(x=" << P.x << "; y=" << P.y << ")";
            return os;
        }
    };
    class ProjectivePoint {
    public:
        mod_int<T> X, Y, Z;
        ProjectivePoint() : X(0), Y(1), Z(0) {}
        ProjectivePoint(const mod_int<T>& X, const mod_int<T>& Y, const mod_int<T>& Z) : X(X), Y(Y), Z(Z) {}
        ProjectivePoint& operator=(ProjectivePoint const& P) { X = P.X; Y = P.Y; Z = P.Z; return *this; };
        explicit operator bool() const { return Z == 0; }
        bool operator==(ProjectivePoint const& P) const { return X * P.Z == P.X * Z && Y * P.Z == P.Y * Z; };
        ProjectivePoint dbl() const {
            if (Y == 0) return ProjectivePoint();
            mod_int<T> W = a*Z*Z + 3*X*X;
            mod_int<T> S = Y*Z;
            mod_int<T> B = X*Y*S;
            mod_int<T> H = W*W - 8*B;
            mod_int<T> sqrS = S*S;
            return ProjectivePoint(2*H*S, W*(4*B-H)-8*Y*Y*sqrS, 8*sqrS*S);
        }
        friend ProjectivePoint operator+ (ProjectivePoint const& P1, ProjectivePoint const& P2)
        {
            if (P1 == ProjectivePoint(0, 1, 0)) return P2;
            if (P2 == ProjectivePoint(0, 1, 0)) return P1;
            if (P1 == P2) return P1.dbl();
            mod_int<T> U1 = P2.Y*P1.Z, U2 = P1.Y*P2.Z, V1 = P2.X*P1.Z, V2 = P1.X*P2.Z;
            if (V1 == V2) {
                if (U1 != U2) return ProjectivePoint();
                else return P1.dbl();
            }
            mod_int<T> U = U1-U2, V = V1-V2, W = P1.Z*P2.Z;
            mod_int<T> sqrV = V*V;
            mod_int<T> cbV = sqrV*V;
            mod_int<T> A = U*U*W - cbV - 2*sqrV*V2;
            return ProjectivePoint(V*A, U*(sqrV*V2 - A), cbV*W);
        }
        friend ProjectivePoint operator*(T const& lb, ProjectivePoint& P) { return fast_mult<T, ProjectivePoint>(lb, P); }
        friend ostream &operator<<(ostream& os, const ProjectivePoint& P)
        {
            os << "(X=" << P.X << " : Y=" << P.Y << " : Z=" << P.Z << ")";
            return os;
        }
    };

};
