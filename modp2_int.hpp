#pragma once

#include <iostream>

#include "mod_int2.hpp"

// T is the int implementation
template <typename T> class modp2_int
{
private: 
    mod_int2<T> v;
    T p;
public:
    modp2_int() : a(0), b(0) {}
    modp2_int(T x, T p) : v(x, p * p), b(b) {}
    T& get_a() { return this.v.get_v() % this.p; }
    T& get_b() { return (this.v.get_v() - this.get_a()) / this.p; }
    friend void swap(modp2_int &x, modp2_int &y) { swap(x.a, y.a); swap(x.b, y.b); }
    friend modp2_int operator+(const modp2_int& x, const modp2_int& y) { return modp2_int(x.v + y.v, x.p); }
    friend modp2_int operator-(const modp2_int& x, const modp2_int& y) { return modp2_int(x.v - y.v, x.p); }
    friend modp2_int operator*(const modp2_int& x, const modp2_int& y) { return modp2_int(x.v * y.v, x.p); }
    friend modp2_int operator*(const T& k, const modp2_int& x) { return modp2_int(k * x.v, x.p); }
    friend modp2_int operator/(const modp2_int& x, const modp2_int& y) { return modp2_int(x.v / y.v, x.p); }
    modp2_int& operator+=(const modp2_int& o) { v += o.v; return *this; }
    modp2_int& operator-=(const modp2_int& o) { v -= o.v; return *this; }
    modp2_int& operator*=(const modp2_int& o) { v *= o.v; return *this; }
    modp2_int& operator/=(const modp2_int& o) { v /= o.v; return *this; }
    bool operator==(const modp2_int& o) const { return o.v == v; }
    bool operator!=(const modp2_int& o) const { return o.v != v; }
    friend std::ostream& operator<<(std::ostream &os, const modp2_int& a) { os << a.v; return os; }
};
