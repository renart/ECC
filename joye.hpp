#include "arithm.hpp"
#include "localringEC.hpp"
#include <iostream>

using namespace std;

typedef mpz_class bint;

typedef mod_int2<bint> modint;
typedef EC<bint> EC;
typedef EC_Z_p2Z<bint> EC2;

bint p;
bint p2(p *p);

EC2::ProjectivePoint lift_y(EC::ProjectivePoint P) {
  P.X /= P.Z;
  P.Y /= P.Z;
  P.Z = modint(1, p);
  bint x(P.X.get_v()), y(P.Y.get_v()), a(EC::a.get_v()), b(EC::b.get_v()),
      p(EC::p.get_v());
  modint delta(((x * x * x + a * x + b - y * y / p) % p2) / p, p);
  return EC2::ProjectivePoint(
      modint(x, p2), modint(y + p * (delta / (2 * modint(y, p))).get_v(), p2),
      modint(1, p2))
}

EC2::ProjectivePoint lift_x(EC::ProjectivePoint P) {
  P.X /= P.Z;
  P.Y /= P.Z;
  P.Z = modint(1, p);
  bint x(P.X.get_v()), y(P.Y.get_v()), a(EC::a.get_v()), b(EC::b.get_v()),
      p(EC::p.get_v());
      modint delta(((x * x * x + a * x + b - y * y / p) % p2) / p, p);
  return EC2::ProjectivePoint(
      modint(x + p * (delta / (modint(3 * x * x + a, p))).get_v(), p2),
      modint(y, p2), modint(1, p2))
}

modint class_function(EC2::ProjectivePoint X, bint q){
  return modint((q * X).X.get_v(), p) / modint(q, p);
}