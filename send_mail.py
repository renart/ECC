#!/usr/bin/env python3
from smtplib import SMTP
import sys
with SMTP("mx-a.polytechnique.fr", 25) as smtp:
    smtp.sendmail('yassir.akram@polytechnique.edu',
                  ['yassir.akram@polytechnique.edu',
                   'louis.abraham@polytechnique.edu'],
                  "Subject: ECM\n" + ' '.join(sys.argv[1:]))
