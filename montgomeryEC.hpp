#pragma once

#include <mpirxx.h>

#include <iostream>
#include <cmath>


// mpz_class dbl = 0;
// mpz_class add = 0;

template <typename T> class EC
{
public:
    static T A;
    static T B;
    static T Ap2s4;
    static std::ostream &strm(std::ostream & os)
    {
        os << B << "Y^2 Z = X(X^2 + " << A << "XZ + Z^2)";
        return os;
    }
    static T delta(){ return A * A - T(4); }
    static void init(T A, T B) {
        EC::A = A;
        EC::B = B;
        EC::Ap2s4 = (A + T(2)) / T(4);
    }

    class xPoint {
    public:
        T X, Z;
        xPoint() : X(1), Z(0) {}
        xPoint(const T& X, const T& Z) : X(X), Z(Z) {}
        xPoint& operator=(xPoint const& P) { X = P.X; Z = P.Z; return *this; };
        bool operator==(xPoint const& P) const { return X * P.Z == P.X * Z; };
        explicit operator bool() const { return (bool)Z; }
        friend void swap(xPoint P, xPoint Q) {
            swap(P.X, Q.X);
            swap(P.Z, Q.Z);
        }
        xPoint xDBL() const {
            if (!*this) return xPoint();
            // dbl++;
            T Q = X + Z, R = X - Z;
            Q *= Q; R *= R;
            T S = Q - R;
            return xPoint(Q * R, S * (R + Ap2s4 * S));
        }
        friend xPoint xADD(xPoint const& xP, xPoint const& xQ, xPoint const& xPmQ)
        {
            if (!xP) return xQ;
            if (!xQ) return xP;
            // if (xP == xQ) return xP.xDBL();
            // add++;
            T U = (xP.X - xP.Z) * (xQ.X + xQ.Z);
            T V = (xP.X + xP.Z) * (xQ.X - xQ.Z);
            T UpV = U + V, UmV = U - V;
            return xPoint(xPmQ.Z * UpV * UpV, xPmQ.X * UmV * UmV);
        }
        template <typename I> friend xPoint ssLADDER(I lb, xPoint const& xP) {
            xPoint x0;
            xPoint x1(xP);
            I lbR(0);
            uint64_t sz(0);
            while (lb) { lbR <<= 1; lbR |= lb & 1; lb >>= 1; sz++; }
            for (; sz; sz--) {
                swapc(x0, x1, I(lbR & 1));
                x1 = xADD(x0, x1, xP);
                x0 = x0.xDBL();
                swapc(x0, x1, I(lbR & 1));
                lbR >>= 1;
            }
            return x0;
        }
        template <typename I> friend xPoint LADDER(I lb, xPoint const& xP) {
            xPoint x0;
            xPoint x1(xP);
            I lbR(0);
            uint64_t sz(0);
            while (lb) { lbR <<= 1; lbR |= lb & 1; lb >>= 1; sz++; }
            for (; sz; sz--) {
                if (I(lbR & 1)) {
                    x0 = xADD(x0, x1, xP);
                    x1 = x1.xDBL();
                } else {
                    x1 = xADD(x0, x1, xP);
                    x0 = x0.xDBL();
                }
                lbR >>= 1;
            }
            // std::cout << dbl << " doubling and " << add << " addings" << std::endl;
            return x0;
        }
        template <typename I> friend xPoint EUCLID2D(I s0, I s1, xPoint const& x) {
            xPoint x0(x), x1(x), xm;
            while (s0) {
                if (s1 < s0) {
                    I st = s0;
                    s0 = s1;
                    s1 = st;
                    swap(x0, x1);
                }
                if (s1 <= 4*s0) {
                    s1 -= s0;
                    xPoint xt = x0;
                    x0 = xADD(x1, x0, xm);
                    swap(xm, xt);
                } else if (I(s0 & 1) == I(s1 & 1)) {
                    s1 = (s1 - s0) >> 1;
                    x0 = xADD(x1, x0, xm);
                    x1 = x1.xDBL();
                } else if (I(s1 & 1) == 0) {
                    s1 >>= 1;
                    xm = xADD(x1, xm, x0);
                    x1 = x1.xDBL();
                } else {
                    s0 >>= 1;
                    xm = xADD(x0, xm, x1);
                    x0 = x0.xDBL();
                }
            }
            while (s1 && I(s1 & 1) == 0) {
                s1 >>= 1;
                x1 = x1.xDBL();
            }
            if (s1 > 1) x1 = LADDER(s1, x1);
            // else std::cout << dbl << " doubling and " << add << " addings" << std::endl;
            return x1;
        }
        template <typename I> friend xPoint PRAC(I s, xPoint xP) {
            while (s && I(s & 1) == 0) {
                s >>= 1;
                xP = xP.xDBL();
            }
            I r = I(s / mpf_class((1. + sqrt(5)) / 2.));
            return EUCLID2D(r, I(s - r), xP);
        }
        template <typename I> friend xPoint operator*(I lb, xPoint const& xP) {
            // dbl = 0;
            // add = 0;
#ifdef ss
            return ssLADDER(lb, xP);
#else
            return LADDER(lb, xP);
#endif
        }
        friend std::ostream &operator<<(std::ostream& os, const xPoint& P)
        {
            os << "(X=" << P.X << " : Z=" << P.Z << ")";
            return os;
        }
    };

};
