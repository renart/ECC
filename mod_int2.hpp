#pragma once

#include <iostream>

template <typename T> class mod_int2
{
private: 
    T v;
	T n;
    inline mod_int2 inv() const {
        T rp = v, up = 1, r = n, u = 0, temp, q;
        while (r)
        {
            q = rp / r;
            rp -= q*r; temp = r; r = rp; rp = temp;
            up -= q*u; temp = u; u = up; up = temp;
        }
        if (rp == -1) { rp *= -1; up *= -1; }
        if (rp != 1) { throw rp; }
        return mod_int2(up % n);
    }
public:
    mod_int2() : v(0), n(1) {}
    mod_int2(T v, T n) : v(v % n), n(n) {}
    T& get_v() { return v; }
    T& get_n() { return n; }
    friend void swap(mod_int2 &a, mod_int2 &b) { swap(a.v, b.v); }
    mod_int2& operator=(const T& a) { v = a % n; return *this; }
    friend mod_int2 operator+(const mod_int2& a, const mod_int2& b) { return mod_int2(a.v + b.v, a.n); }
    friend mod_int2 operator-(const mod_int2& a, const mod_int2& b) { return mod_int2(a.v - b.v, a.n); }
    friend mod_int2 operator*(const mod_int2& a, const mod_int2& b) { return mod_int2(a.v * b.v, a.n); }
    friend mod_int2 operator*(const T& k, const mod_int2& a) { return mod_int2(k * a.v, a.n); }
    friend mod_int2 operator/(const mod_int2& a, const mod_int2& b) { return a * b.inv(); }
    mod_int2 operator-() { return mod_int2(-v, n); }
    mod_int2& operator+=(const mod_int2& a) { v = (v + a.v) % n; return *this; }
    mod_int2& operator-=(const mod_int2& a) { v = (v - a.v) % n; return *this; }
    mod_int2& operator*=(const mod_int2& a) { v = (v * a.v) % n; return *this; }
    mod_int2& operator/=(const mod_int2& a) { *this *= a.inv(); return *this; }
    bool operator==(const mod_int2& a) const { return a.v == v; }
    bool operator!=(const mod_int2& a) const { return a.v != v; }
    explicit operator bool() const { return (bool)v; }
    friend std::ostream& operator<<(std::ostream &os, const mod_int2& a) { os << (a.v + n) % n; return os; }
};
