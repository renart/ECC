#pragma once

#include <iostream>

template <typename T> class mod_int
{
private: 
    T v;
    inline mod_int inv() const {
        T rp = v, up = 1, r = n, u = 0, temp, q;
        while (r)
        {
            q = rp / r;
            rp -= q*r; temp = r; r = rp; rp = temp;
            up -= q*u; temp = u; u = up; up = temp;
        }
        if (rp == -1) { rp *= -1; up *= -1; }
        if (rp != 1) { throw rp; }
        return mod_int(up % n);
    }
public:
    static T n;
    static void set_n(T n) { mod_int::n = n;}
    mod_int() : v(0) {}
    mod_int(T v) : v(v % n) {}
    T& get_v() { return v; }
    friend void swap(mod_int &a, mod_int &b) { swap(a.v, b.v); }
    mod_int& operator=(const T& a) { v = a % n; return *this; }
    friend mod_int operator+(const mod_int& a, const mod_int& b) { return mod_int(a.v + b.v); }
    friend mod_int operator-(const mod_int& a, const mod_int& b) { return mod_int(a.v - b.v); }
    friend mod_int operator*(const mod_int& a, const mod_int& b) { return mod_int(a.v * b.v); }
    friend mod_int operator*(const T& k, const mod_int& a) { return mod_int(k * a.v); }
    friend mod_int operator/(const mod_int& a, const mod_int& b) { return a * b.inv(); }
    mod_int operator-() { return mod_int(-v); }
    mod_int& operator+=(const mod_int& a) { v = (v + a.v) % n; return *this; }
    mod_int& operator-=(const mod_int& a) { v = (v - a.v) % n; return *this; }
    mod_int& operator*=(const mod_int& a) { v = (v * a.v) % n; return *this; }
    mod_int& operator/=(const mod_int& a) { *this *= a.inv(); return *this; }
    bool operator==(const mod_int& a) const { return a.v == v; }
    bool operator!=(const mod_int& a) const { return a.v != v; }
    explicit operator bool() const { return (bool)v; }
    friend std::ostream& operator<<(std::ostream &os, const mod_int& a) { os << (a.v + n) % n; return os; }
};
