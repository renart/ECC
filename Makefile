all: X25519 lenstra lenstra_MPI

INC = /users/eleves-b/2015/yassir.akram/.local/include/
LIB = /users/eleves-b/2015/yassir.akram/.local/lib/

X25519: X25519.cpp montgomeryEC.hpp arithm.hpp mod_int.hpp
	g++ -std=gnu++11 -Wall -O3 -I$(INC) -L$(LIB) -lmpirxx -lmpir X25519.cpp -o X25519

lenstra: lenstra.cpp int_types.hpp montgomeryEC.hpp arithm.hpp mod_int.hpp send_mail.py
	g++ -std=gnu++11 -Wall -O3 -I$(INC) -L$(LIB) -lmpirxx -lmpir lenstra.cpp -o lenstra

lenstra_MPI: lenstra.cpp int_types.hpp montgomeryEC.hpp arithm.hpp mod_int.hpp send_mail.py
	mpic++ -std=gnu++11 -Wall -O3 -I$(INC) -L$(LIB) -DUSE_MPI -lmpi -lmpirxx -lmpir lenstra.cpp -o lenstra_MPI

clean:
	rm -rf X25519 lenstra lenstra_MPI
