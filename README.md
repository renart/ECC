Cryptology on Elliptic curves
=============================

by Louis Abraham and Yassir Akram

Description
-----------

This library implements X25519 Diffie-Hellman key-exchange algorithm,
and Lenstra's ECM.

Curves Arithmetic
-----------------

We define affine and projective arithmetic on Weirstrass curves, and
x-projective arithmetic on Montgomery curves.

### Templates

The code is written using templates, thus allowing us to switch between 
integers and curves types very easily.


### Point integer multiplication on Montgomery curves

We defined three ways to multiply an XPoint.
-   a secure ways, activated using flag `ss`
-   Montgomery's Ladder's algorithm
-   PRAC's algorithm

X25519
------

Our first binary is X25519. On top of the X25519, the main function
tests if the same value is found by Diffie-Hellman key-exchange
algorithm.

Lenstra ECM
-----------

### Algorithm

We used Lenstra's algorithm on Montgomery elliptic curves, despite we
started by using Weirstrass curves.

At the beginning, we do a sieve up to $10^7$.

To find a factor, we select random Montgomery curves, until a factor is
found.

To factorize a number, we take off small prime numbers - up to $10^7$.
We then keep looking for factors -calling back factorize on those
factors-, until the remaining number becomes pseudo-prime.
Miller-Rabin's probabilistic pseudo-primality test is used with an error
probability of $4^{-25}$.

### Second stage

Although the second stage has been implementated, we choosed not to use it:
in fact, it is theoretically and practically tougher to tune.

### Types choice

For the integers, we used the [MPIR library](http://mpir.org/) because
it allows multiple precision integers. For the size of the given inputs,
it is also possible to use 512-bit integers (like the `int512_t` from
[boost](http://www.boost.org/doc/libs/1_62_0/libs/multiprecision/doc/html/boost_multiprecision/tut/ints/cpp_int.html)).
Our tests show that although the operations are a bit faster with
dynamic types (like `mpz_t`), the variable affectation is much slower.
Thus, we tried allocating them statically, and tried to used static
types (with fixed length like `int512_t`), but there was almost no
perceptible performance gain. This may be explained by the fact that 
MPIR is a bit more optimized for moduli operations. Maybe defining 
a specific type for this usage can yield to better performances.

Using the dynamic approach, we observed similar performances with the 
builtin integers from Python.

To switch from one type to another, one has only to change the definition of 
`bint` in `lenstra.cpp`.

### Parallel Lenstra

We parallelized our algorithm using MPI. Our code compiles
differently according to the flag `USE_MPI`. Two exutables are 
created:

Our protocol is as follows:

-   The root node (with rank 0) broadcasts (`MPI_Bcast`) a composite
    number
-   The child nodes (with rank \> 0) try random elliptic curves.
-   When one child node finds a factor, it sends it to the root node.
-   The root node stops the child nodes after their attempt ends,
    retrieves the factors they send if any, and divides its number by
    them.
-   While the number is not a pseudoprime, the algorithm loops.

### Results

All numbers up to challenge 35 where very quick to factorize. Challenge 40
took a whole day to be solved, being parallelized on 50 nodes. We also tested 
approximately 20000 curves for challenge 45, unsuccessfully.