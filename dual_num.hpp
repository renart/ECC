#pragma once

#include <iostream>
  
// T is the base field
template <typename T> class dual_num
{
private: 
    T a, b;
    inline dual_num inv() const {
        T c(a.inv());
        T d(- b * c * c);
        return dual_num(c, d);
    }
public:
    dual_num() : a(0), b(0) {}
    dual_num(T x, T y) : a(x), b(y) {}
    T& get_a() { return a; }
    T& get_b() { return b; }
    friend void swap(dual_num &x, dual_num &y) { swap(x.a, y.a); swap(x.b, y.b); }
    friend dual_num operator+(const dual_num& x, const dual_num& y) { return dual_num(x.a + y.a, x.b + y.b); }
    friend dual_num operator-(const dual_num& x, const dual_num& y) { return dual_num(x.a - y.a, x.b - y.b); }
    friend dual_num operator*(const dual_num& x, const dual_num& y) { return dual_num(x.a * y.a, x.a * y.b + x.b * y.a); }
    friend dual_num operator*(const T& k, const dual_num& a) { return dual_num(k * a.a, k * a.b); }
    friend dual_num operator/(const dual_num& a, const dual_num& b) { return a * b.inv(); }
    dual_num& operator+=(const dual_num& o) { a += o.a; b += o.b; return *this; }
    dual_num& operator-=(const dual_num& o) { a -= o.a; b -= o.b; return *this; }
    dual_num& operator*=(const dual_num& o) { *this = this * o; return *this; } // not optiTzed
    dual_num& operator/=(const dual_num& o) { *this *= o.inv(); return *this; }
    bool operator==(const dual_num& o) const { return (o.a == a) && (o.b == b); }
    bool operator!=(const dual_num& o) const { return (o.a != a) || (o.b != b); }
    friend std::ostream& operator<<(std::ostream &os, const dual_num& a) { os << a.a << ' + ' << a.b << 'ε'; return os; }
};
