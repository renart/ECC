#include "montgomeryEC.hpp"
#include "arithm.hpp"
#include "mod_int.hpp"

#include <mpirxx.h>
#include <ctime>
#include <chrono>

#define ss

template<> mpz_class mod_int<mpz_class>::n = mpz_class();
template<> mod_int<mpz_class> EC<mod_int<mpz_class>>::A = mod_int<mpz_class>();
template<> mod_int<mpz_class> EC<mod_int<mpz_class>>::B = mod_int<mpz_class>();
template<> mod_int<mpz_class> EC<mod_int<mpz_class>>::Ap2s4 = mod_int<mpz_class>();

mod_int<mpz_class> X25519(mpz_class m, mod_int<mpz_class> u) {
	EC<mod_int<mpz_class>>::xPoint mPu = m * EC<mod_int<mpz_class>>::xPoint(u, mod_int<mpz_class>(1));
	return mPu.X * fast_pow<mod_int<mpz_class>, mpz_class>(mPu.Z, mod_int<mpz_class>::n - 2);
}


#include <iostream>
using namespace std;
using namespace std::chrono;

int main() {	
	mod_int<mpz_class>::set_n((mpz_class(1) << 255) - 19);
	EC<mod_int<mpz_class>>::init(mpz_class(486662), mpz_class(1));
	gmp_randclass rc(gmp_randinit_default);
	rc.seed(time_point_cast<milliseconds>(system_clock::now()).time_since_epoch().count());
	mod_int<mpz_class> u1(9);
	mpz_class a = rc.get_z_bits(251) << 3;
	mpz_class b = rc.get_z_bits(251) << 3;
	mpz_setbit(a.get_mpz_t(), 254);
	mpz_setbit(b.get_mpz_t(), 254);
	mod_int<mpz_class> ua = X25519(a, u1);
	mod_int<mpz_class> ub = X25519(b, u1);
	cout << X25519(b, ua) << endl;
	cout << X25519(a, ub) << endl;
}