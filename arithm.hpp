#pragma once

#include "arithm.hpp"
#include "mod_int.hpp"


template <typename I, typename T> T fast_mult(I d, T a);
template <typename T, typename I> T fast_pow(T a, I d);
template <typename T> bool is_prime(T n);
template <typename T> T rand_int(T min, T max);
template <typename T> bool is_pseudo_prime(T const& N, unsigned int t = 25);
template <typename T> T pgcd(T a, T b);
template <typename T> T ppcm(T a, T b);


template <typename I, typename T> T fast_mult(I d, T a) {
    if (d == 0 || a == T()) return T();
    if (d == 1) return a;
    T r = T();
    while (true)
    {
        if (d % 2 == 1)
            r = (r + a);
        if ((d >>= 1) == 0) break;
        a = a + a;
    }
    return r;
}

template <typename T, typename I> T fast_pow(T a, I d) {
    if (d == 1) return a;
    if (d == 0 && a != T(0)) return T(1);
    if (a == (T)1) return T(1);
    // invariant à l'entrée: a^d[n]=r*(tmp=a*2^k)^{d=E(d_0/2^k)}
    T r = T(1);
    T tmp = a; // a^{2^r}
    while (d != 0)
    {
        if (d % 2 == 1) r = (r * tmp);
        tmp = (tmp * tmp);
        d >>= 1;
    }
    return r;
}

template <typename T> bool is_prime(T n) {
    if (n < 2) return false;
    if (n == 2) return true;
    for (int i = 2; i <= sqrt(n); i++) { if (n % i == 0) return false; }
    return true;
}

template <typename T> T rand_int(T min, T max) {
    return (T)rand() % (max - min) + min;
}




template <typename T> bool is_pseudo_prime(T const& N, unsigned int t) {
    if (N < 2) return false;
    if (N == 2) return true;
    if (N % 2 == 0) return false;
    if (log10(N) <= 7) return is_prime<T>(N);

    for (int d = 3; d <= log10(N); d++)
    {
        if (N % d == 0) return 0;
    }

    T d = N - 1; int s = 0;
    while (d % 2 == 0) { d >>= 1; s++; }

    mod_int<T>::n = N;
    mod_int<T> ad;
    while (t--)
    {
        ad = rand_int<T>(2, N - 1);
        ad = fast_pow<mod_int<T>, T>(ad, d);
        if (ad == mod_int<T>(1)) continue;
        int s_tmp = s;
        while (ad != mod_int<T>(N-1) && s_tmp--) ad = ad * ad;
        if (ad != mod_int<T>(N-1)) return false;
    }
    return true;
}

template <typename T> T pgcd(T a, T b)
{
    if (a < b) {
        T r = a;
        a = b;
        b = r;
    }
    while (b)
    {
        T r = a % b;
        a = b;
        b = r;
    }
    if (a < 0) a *= T(-1);
    return a;
}

template <typename T> T ppcm(T a, T b)
{
    return a*b / pgcd(a, b);
}
