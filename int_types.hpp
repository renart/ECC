#include <mpirxx.h>
#include <boost/multiprecision/cpp_int.hpp>
#include <boost/random.hpp>
#include <cmath>

using namespace boost::multiprecision;
using namespace boost::random;

uint64_t log2(mpz_class n) {
    return mpz_sizeinbase(n.get_mpz_t(), 2) + 1;
}
uint64_t log2(int512_t n) {
    int log2 = 1;
    while (n) {
        n >>= 1;
        log2++;
    }
    return log2;
}

template <typename T> double log10(T n) {
    return log(2) * (double)log2(n);
}

void swap(uint64_t &A, uint64_t &B) {
    uint64_t T = A;
    A = B;
    B = T;
}
void swap(mpz_class &A, mpz_class &B) {
    mpz_swap(A.get_mpz_t(), B.get_mpz_t());
}
void swap(int512_t &A, int512_t &B) {
    int512_t T = A;
    A = B;
    B = T;
}

template <typename T> void swapc(T &A, T &B, T b) {
    T v = b  & (A ^ B);
    A ^= v;
    B ^= v;
}


char* get_str(char data[], int512_t const& n) {
    return 0;
}

char* get_str(char data[], mpz_class const& n) {
    return mpz_get_str(data, 10, n.get_mpz_t());
}


gmp_randclass rc(gmp_randinit_default);
mpz_class randint(mpz_class const &min, mpz_class const &max) {
    return rc.get_z_range(mpz_class(max - min)) + min;
}
/*
mt19937 mt;
int512_t randint(int512_t const &min, int512_t const &max) {
    uniform_int_distribution<cpp_int> ui(min, max);
    return int512_t(ui(mt));
}
*/

