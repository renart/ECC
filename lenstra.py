from random import randint
from math import sqrt, log, exp
from time import time

# N = 513647030594488887305873717502131586188356081480550566904610612180932741351421561189852 // (2 * 2 * 3 * 229 * 1721)
N = 1012869169588026491953203200982007009363 * 107229388935655927705848145865664713542363


def pgcd(a, b):
    while b:
        a, b = b, a % b
    return a


maxN = 10**7
iscomp = [False]*maxN
primes = []
for p in range(2, maxN):
    if not iscomp[p]:
        primes.append(p)
        for k in range(2*p, maxN, p):
            iscomp[k] = True


logp = 1/2 * log(N)
B = exp(1. / sqrt(2) * sqrt(logp * log(logp)))
k = 1
i = 0
while primes[i] <= B:
    p = primes[i]
    pri = 1
    while pri * p <= B:
        pri *= p
    k *= pri
    i += 1


def xADD(P, Q, PmQ):
    U = ((P[0] - P[1]) * (Q[0] + Q[1])) % N
    V = ((P[0] + P[1]) * (Q[0] - Q[1])) % N
    return ((PmQ[1] * (U + V) * (U + V)) % N, (PmQ[0] * (U - V) * (U - V)) % N)

def xDBL(P):
    Q = ((P[0] + P[1]) * (P[0] + P[1])) % N
    R = ((P[0] - P[1]) * (P[0] - P[1])) % N
    S = (Q - R) % N
    return((Q * R) % N, (S * (R + S * Am2p4)) % N)

def ladder(m, P):
    binarM = bin(m)[2:]
    u = P
    x0,x1 = (1,0),u
    for mi in binarM:
        if mi=='0':
            x1 = xADD(x0, x1, u)
            x0 = xDBL(x0)
        else:
            x0 = xADD(x0, x1, u)
            x1 = xDBL(x1)
    return x0

def lenstra():
    start = time()
    
    nbTest = 0
    while True:
        nbTest += 1
        sg = randint(5, N-1)
        u, v = (sg * sg - 5) % N, (4 * sg) % N
        vmu = (v - u) % N
        uc, vc = (((u * u) % N) * u) % N, (((v * v) % N) * v) % N
        A = (vmu * vmu * vmu * (3 * u + v) * pow(4 * uc * v, N - 2, N) - 2) % N
        P = (uc, vc)
        Am2p4 = ((A + 2) * pow(4, N - 2, N)) % N
        kP = ladder(k, P)
        gcd = pgcd(N, kP[1])
        if gcd != 1:
            break
        print(nbTest, "tests" if nbTest > 1 else "test")
    
    end = time()
    
    print("Per curve:", (end-start)/nbTest, "s")
    print(gcd)

P = (12001790465623363353694177186031242813212476974723026591949086732349255434655386, 71481590798422993646179755465790290673208722435690881728305378019396383406171738)
kP = ladder(k, P)


#N=38608114493665927502670583313661475427960953070443260656439240369935449588118767819468482995650761519977593
#V=10422858976141239336746214695912607693412316169136426433304682894455488921862742471396070704950320009